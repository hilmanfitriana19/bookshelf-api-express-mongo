import mongoose from "mongoose";
import { producer } from "../broker/kafka";
import redisClient from "../cache/redis";
import { logger } from "../logger/logger";
import bookService from "../service/book.service";
import { MongoErrorCode } from "../utils/constant/mongodb.error";
import { ResponseMessage } from "../utils/constant/response.message.constant";
import { responseUtil } from "../utils/response/response.utils";
import { bookValidator } from "../utils/validator/book.validator";
import { queryValidator } from "../utils/validator/query.validator";


const bookController = ({
    create: async (req, res) => {

        const body = req.body;

        try {

            logger.info(`Request Create New Book Data with data ${JSON.stringify(body)}`)
            const validationResult = bookValidator.create.validate(body, { allowUnknown: true });
            if (validationResult.error) {
                logger.error(`Fail Create Book : ${validationResult.error.message}`);
                return responseUtil.FailProcess(res, validationResult.error.message);
            }

            const book = await bookService.create(body);
            logger.info(`Create Book Data Success`)
            delete book._doc.users
            return responseUtil.SuccessCreated(res, book);
        } catch (e) {
            logger.error(`Request Create New Book Data Failed : ${e?.meesage || e}`)
            if (e.code === MongoErrorCode.DUPLICATE) return responseUtil.FailProcess(res, ResponseMessage.NameBookTaken)
            return responseUtil.InternalServerError(res)
        }
    },

    getAll: async (req, res) => {

        const offset = req.query.offset;
        const limit = req.query.limit;


        logger.info(`Request Get List Book start from index ${offset || 0} limit : ${limit || '-'} `)

        const validationResult = queryValidator.validate({ offset, limit }, { allowUnknown: true })
        if (validationResult.error) {
            logger.error(`Fail Get List Book : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res, validationResult.error.message);
        }

        const cacheResult = await redisClient.get(`BookList_${offset || 0}_${limit}`)
        if (cacheResult) {
            logger.info(`Set response from redis Cache`)
            producer.sendData('test-kafka', cacheResult)
            return res.send(JSON.parse(cacheResult))
        } else {
            logger.info(`Set Response From Database`)
            const result = await bookService.getAll(offset, limit);
            redisClient.setData(`BookList_${offset || 0}_${limit}`, JSON.stringify(result))
            producer.sendData('test-kafka', JSON.stringify(result))
            return res.send(result);
        }
    },

    getOne: async (req, res) => {

        const { id } = req.params
        logger.info(`Request Get Book Data with id : ${id} `)

        try {
            const result = await bookService.getOneById(id);
            logger.info(`Request Get Book Data Success with id: ${id} Success`)
            return res.send(result);
        } catch (e) {
            logger.error(`Request Create New Book Data Failed:${e?.meesage || e} `)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    },

    update: async (req, res) => {
        const { id } = req.params;
        const body = req.body;

        try {
            logger.info(`Request Update Book Data with id : ${id} with data ${JSON.stringify(body)}`)

            const validationResult = bookValidator.update.validate(body, { allowUnknown: true });
            if (validationResult.error) {
                logger.error(`Fail Update Book : ${validationResult.error.message}`);
                return responseUtil.FailProcess(res, validationResult.error.message);
            }
            await bookService.getOneById(id);

            const book = await bookService.update(id, body);
            logger.info(`Request Update Book Data Success id: ${id} `)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessUpdated, book)
        } catch (e) {
            logger.error(`Request Update Book Data Failed:${e?.meesage || e} `)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            if (e.code === MongoErrorCode.DUPLICATE)
                return responseUtil.FailProcess(res, ResponseMessage.NameBookTaken)
            return responseUtil.InternalServerError(res)
        }
    },


    delete: async (req, res) => {
        const { id } = req.params;
        logger.info(`Request Delete Book Data with id : ${id} `)
        try {
            await bookService.getOneById(id);

            const book = await bookService.delete(id);

            logger.info(`Request Delete Book Data Success id: ${id} `)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessDeleted, book)

        } catch (e) {
            logger.error(`Request Delete Book Data Failed:${e?.meesage || e} `)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    }
})


export default bookController;