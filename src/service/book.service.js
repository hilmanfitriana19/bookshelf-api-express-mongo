const { Book } = require("../model/book.model")

const bookService = ({

    create: (body) => {
        const book = new Book(body);
        return book.save();
    },

    getAll: (offset, limit) => {

        return Book.find().select('-users').skip(offset || 0).limit(limit || null);
    },

    getOneById: (id) => {
        return Book.findById(id).select('-users').orFail();
    },

    update: (id, body) => {
        return Book.findOneAndUpdate({ _id: id }, body, { returnOriginal: false, select: '-users' })
    },

    delete: (id) => {
        return Book.findOneAndDelete({ _id: id }, { select: '-users' })
    }

})

export default bookService;