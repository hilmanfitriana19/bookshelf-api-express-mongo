import { getMaxAge } from "../src/utils/helper/cookie.helper";

describe('Check Get Max Age', () => {
    it('Test using Days should return correct number', () => {
        expect(getMaxAge('2d')).toBe(172800000)
    });
    it('Test using Hour should return correct number', () => {
        expect(getMaxAge('5h')).toBe(18000000)
    });
    it('Test using Minutes should return correct number', () => {
        expect(getMaxAge('10m')).toBe(600000)
    });
    it('Test using Second should return correct number', () => {
        expect(getMaxAge('100s')).toBe(100000)
    });
    it('Test using Year should return correct number', () => {
        expect(getMaxAge('1y')).toBe(31536000000)
    });
    it('Test non Exist Time Indicator should return correct number', () => {
        expect(getMaxAge('2t')).toBe(0)
    });
});