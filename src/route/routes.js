import express from 'express';
import 'express-group-routes';
import AuthController from '../controller/auth.controller';
import bookController from '../controller/book.controller';
import userBookController from '../controller/user-book.controller';
import userController from '../controller/user.controller';
import { JwtAuth } from '../middleware/authentication';


const routes = express();

routes.group('/book', (router) => {
    router.post('/', JwtAuth, bookController.create)
    router.get('/', bookController.getAll)
    router.get('/:id', bookController.getOne)
    router.patch('/:id', JwtAuth, bookController.update)
    router.delete('/:id', JwtAuth, bookController.delete)
})

routes.group('/user', (router) => {
    router.post('/', JwtAuth, userController.create)
    router.get('/', userController.getAll)
    router.get('/detail', JwtAuth, userController.getAllDetail);
    router.get('/:id', JwtAuth, userController.getOne)
    router.get('/:id/detail', JwtAuth, userController.getOneDetail);
    router.patch('/:id', JwtAuth, userController.update)
    router.delete('/:id', JwtAuth, userController.delete)
})

routes.group('/user-book', (router) => {

    router.post('/', JwtAuth, userBookController.create);
    router.get('/', JwtAuth, userBookController.getAll);
    router.get('/:id', JwtAuth, userBookController.getOne);
    router.patch('/:id', JwtAuth, userBookController.update);
    router.delete('/:id', JwtAuth, userBookController.delete);
});

routes.group('/auth', (router) => {
    router.post('/login', AuthController.login)
    router.post('/refresh', AuthController.refreshToken)
    router.post('/logout', AuthController.logout)
})

export default routes;