import env from './config/env';
import * as db from './database';
import app from './app';

const port = env.PORT

db.connectDB()
app.listen(port, () => {
    console.log(`Listen to Port ${port}`);
})