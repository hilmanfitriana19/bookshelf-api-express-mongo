import request from 'supertest'
import app from '../src/app'
import { ResponseMessage } from '../src/utils/constant/response.message.constant'
import * as mongo from '../src/utils/test-utils'
import redisClient from '../src/cache/redis'

const book1 = {
    name: "Coming From tomorrow",
    summary: "Coming from tomorrow to meet you in the past",
    publisher: "Dadang Konelo",
    author: 'Dadang Konelo',
    pageCount: 30,
    year: 2006
}
const book2 = {
    name: "Coming From Yesterday",
    author: 'Hilman',
    pageCount: 30
}
const book3 = {
    name: "Coming From Last Year",
    summary: "I am Coming From Last Year to Meet You",
    publisher: "Media Internautika",
    year: 2001
}

const offset = 0
const limit = 2

let currentId = null


jest.mock('../src/middleware/authentication', () => ({
    JwtAuth: jest.fn((req, res, next) => next()),
}))

beforeAll(async () => {
    await mongo.connectDB()

    jest.spyOn(redisClient, 'get').mockImplementation((name) => { return null })
})

afterAll(async () => {
    mongo.disconnectDB()
})


describe('Book User API ', () => {
    describe('Create Book', () => {
        afterAll(async () => {
            await mongo.dropCollections()
        })

        describe('Using Correct Request Body', () => {
            it('Should Return Created Book', async () => {
                const result = await request(app).post('/book/').send(book1)

                expect(result.status).toBe(201)
                expect(result.body.data).toMatchObject(book1)
                expect(result.body.message).toBe(ResponseMessage.SuccessCreated)
                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('name')
                expect(result.body.data).toHaveProperty('author')
                expect(result.body.data).toHaveProperty('year')
                expect(result.body.data).toHaveProperty('summary')
                expect(result.body.data).toHaveProperty('publisher')
                expect(result.body.data).toHaveProperty('pageCount')
                expect(result.body.data).not.toHaveProperty('users')

            });
        });

        describe('Using Invalid Request Body Type', () => {
            it('Non Existing Name Should Return Error', async () => {
                const result = await request(app).post('/book/').send({ 'author': 'author', 'year': 2010, 'summary': 'summary', 'publisher': 'publisher', 'pageCount': 20 })
                expect(result.body.error).toBe('name is required')
                expect(result.status).toBe(400)
            });

            it('Non String Name Should Return Error', async () => {
                const result = await request(app).post('/book/').send({ 'name': 12, 'author': 'author', 'year': 2010, 'summary': 'summary', 'publisher': 'publisher', 'pageCount': 20 })
                expect(result.body.error).toBe('name must be string')
                expect(result.status).toBe(400)
            });

            it('Empty String Name Should Return Error', async () => {
                const result = await request(app).post('/book/').send({ 'name': '', 'author': 'author', 'year': 2010, 'summary': 'summary', 'publisher': 'publisher', 'pageCount': 20 })
                expect(result.body.error).toBe('name cannot be empty')
                expect(result.status).toBe(400)
            });

            it('Non String Author Should Return Error', async () => {
                const result = await request(app).post('/book/').send({ 'name': 'book name', 'author': 12, 'year': 2010, 'summary': 'summary', 'publisher': 'publisher', 'pageCount': 20 })
                expect(result.body.error).toBe('author must be string')
                expect(result.status).toBe(400)
            });

            it('Non String Summary Should Return Error', async () => {
                const result = await request(app).post('/book/').send({ 'name': 'book name', 'author': 'author', 'year': 2010, 'summary': 12345, 'publisher': 'publisher', 'pageCount': 20 })
                expect(result.body.error).toBe('summary must be string')
                expect(result.status).toBe(400)
            });

            it('Non String Publisher Should Return Error', async () => {
                const result = await request(app).post('/book/').send({ 'name': 'book name', 'author': 'author', 'year': 2010, 'summary': 'summary', 'publisher': 4342, 'pageCount': 20 })
                expect(result.body.error).toBe('publisher must be string')
                expect(result.status).toBe(400)
            });

            it('Non Number Year Should Return Error', async () => {
                const result = await request(app).post('/book/').send({ 'name': 'book name', 'author': 'author', 'year': '2020aaaa', 'summary': 'summary', 'publisher': '', 'pageCount': 20 })
                expect(result.body.error).toBe('year must be number')
                expect(result.status).toBe(400)
            });

            it('Non Number Page Count Should Return Error', async () => {
                const result = await request(app).post('/book/').send({ 'name': 'book name', 'author': 'author', 'year': 2020, 'summary': 'summary', 'publisher': '', 'pageCount': 'hexagonal' })
                expect(result.body.error).toBe('pageCount must be number')
                expect(result.status).toBe(400)
            });
        });

        describe('Using Exisiting Request Body', () => {
            it('Using Name already Taken should Return Error', async () => {
                const result = await request(app).post('/book/').send({ ...book2, name: book1.name })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe(ResponseMessage.NameBookTaken)
            });
        });
    });

    describe('Get List Book', () => {
        beforeAll(async () => {
            await mongo.db.collection('books').insertMany([{ ...book1 }, { ...book2 }, { ...book3 }])
        })

        afterAll(async () => {
            await mongo.dropCollections()
        })

        describe('Without Offset and Limit', () => {
            it('should Return List Book', async () => {
                const result = await request(app).get('/book/')
                expect(result.status).toBe(200)

                expect(result.body).toBeInstanceOf(Array)
                expect(result.body[0]).toHaveProperty('id')
                expect(result.body[0]).toHaveProperty('name')
                expect(result.body[0]).toHaveProperty('author')
                expect(result.body[0]).toHaveProperty('year')
                expect(result.body[0]).toHaveProperty('summary')
                expect(result.body[0]).toHaveProperty('publisher')
                expect(result.body[0]).toHaveProperty('pageCount')
                expect(result.body[0]).not.toHaveProperty('users')

                expect(result.body[1]).toHaveProperty('id')
                expect(result.body[1]).toHaveProperty('name')
                expect(result.body[1]).toHaveProperty('author')
                expect(result.body[1]).toHaveProperty('year')
                expect(result.body[1]).toHaveProperty('summary')
                expect(result.body[1]).toHaveProperty('publisher')
                expect(result.body[1]).toHaveProperty('pageCount')
                expect(result.body[1]).not.toHaveProperty('users')

                expect(result.body[2]).toHaveProperty('id')
                expect(result.body[2]).toHaveProperty('name')
                expect(result.body[2]).toHaveProperty('author')
                expect(result.body[2]).toHaveProperty('year')
                expect(result.body[2]).toHaveProperty('summary')
                expect(result.body[2]).toHaveProperty('publisher')
                expect(result.body[2]).toHaveProperty('pageCount')
                expect(result.body[2]).not.toHaveProperty('users')

                expect(result.body[0]).toMatchObject(book1)
                expect(result.body[1]).toMatchObject(book2)
                expect(result.body[2]).toMatchObject(book3)

                expect(result.body.length).toBe(3)
            });
        });
        describe('From Redis Cache', () => {
            afterAll(async () => {
                jest.spyOn(redisClient, 'get').mockImplementation((name) => { return null })
            })


            it('should Return List User', async () => {
                const mockRedis = jest.spyOn(redisClient, 'get').mockImplementation((name) => { return JSON.stringify([book1, book2]) })
                const result = await request(app).get('/book/')

                expect(mockRedis).toHaveBeenCalled()
                expect(mockRedis).toBeCalledWith('BookList_0_undefined')
                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Array)
                expect(result.body.length).toBe(2)

                expect(result.body[0]).toMatchObject(book1)
                expect(result.body[1]).toMatchObject(book2)

            });
        });

        describe('Using Offset and Limit Data', () => {
            describe('Using Number in Offset and Limit', () => {
                it('Should Return List User', async () => {
                    const result = await request(app).get('/book/')
                        .query({ offset, limit })

                    expect(result.status).toBe(200)
                    expect(result.body).toBeInstanceOf(Array)
                    expect(result.body.length).toBe(limit)
                    expect(result.body[0]).toMatchObject(book1)
                    expect(result.body[1]).toMatchObject(book2)
                })
            })

            describe('Using Non Number Offset', () => {
                it('Should Return Error Message', async () => {
                    const result = await request(app).get('/book/')
                        .query({ offset: 'lima', limit })

                    expect(result.body.error).toEqual('offset must be number')
                    expect(result.status).toBe(400)
                })
            })
            describe('Using Non Number Limit', () => {
                it('Should Return Error Message', async () => {
                    const result = await request(app).get('/book/')
                        .query({ offset, limit: 'lima' })

                    expect(result.body.error).toEqual('limit must be number')
                    expect(result.status).toBe(400)
                })
            })
        });
    });

    describe('Get One Book By Id', () => {

        beforeAll(async () => {
            const book = await mongo.db.collection('books').insertOne({ ...book1 });
            currentId = book.insertedId.toString()
        })

        afterAll(async () => {
            await mongo.dropCollections();
        })

        describe('Using Existing Id', () => {
            it('should Return Book Data', async () => {
                const result = await request(app).get(`/book/${currentId}`)

                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Object)
                expect(result.body).toMatchObject(book1)
                expect(result.body).toHaveProperty('id')
                expect(result.body).toHaveProperty('name')
                expect(result.body).toHaveProperty('author')
                expect(result.body).toHaveProperty('year')
                expect(result.body).toHaveProperty('summary')
                expect(result.body).toHaveProperty('publisher')
                expect(result.body).toHaveProperty('pageCount')
                expect(result.body).not.toHaveProperty('users')
            })
        });

        describe('Using Non Existing Id', () => {
            it('Should Return Error 404', async () => {
                const result = await request(app).get(`/book/nonExistingId`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)

            })
        });
    });


    describe('Update Book By Id', () => {

        beforeAll(async () => {
            const book = await mongo.db.collection('books').insertOne({ ...book1 });
            await mongo.db.collection('books').insertOne({ ...book2 });
            currentId = book.insertedId.toString()
        })

        afterAll(async () => {
            await mongo.dropCollections();
        })

        describe('Using Existing Id', () => {
            it('should Return Success Response and Updated Data', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'publisher': 'A New Publisher' })

                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)

                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('name')
                expect(result.body.data).toHaveProperty('author')
                expect(result.body.data).toHaveProperty('year')
                expect(result.body.data).toHaveProperty('summary')
                expect(result.body.data).toHaveProperty('publisher')
                expect(result.body.data).toHaveProperty('pageCount')

                expect(result.body.data.publisher).toBe('A New Publisher')
                expect(result.body.message).toBe(ResponseMessage.SuccessUpdated)
            })
        });

        describe('Using Non Invalid Request', () => {
            it('Non Exist Id Should Return Error 404', async () => {
                const result = await request(app).patch(`/book/nonExistingId`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)
            })

            it('Update Name to Taken Name Should Return Error', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'name': book2.name })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe(ResponseMessage.NameBookTaken)

            })

            it('Update Name to Non String Should Return Error', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'name': 12345 })

                expect(result.body.error).toBe('name must be string')
                expect(result.status).toBe(400)

            })

            it('Update Name to Non String Should Return Error', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'name': '' })

                expect(result.body.error).toBe('name cannot be empty')
                expect(result.status).toBe(400)

            })

            it('Update Author to Non String Should Return Error', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'author': 1234 })

                expect(result.body.error).toBe('author must be string')
                expect(result.status).toBe(400)

            })

            it('Update Summary to Non String Should Return Error', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'summary': 1234 })

                expect(result.body.error).toBe('summary must be string')
                expect(result.status).toBe(400)

            })

            it('Update Publisher to Non String Should Return Error', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'publisher': 1234 })

                expect(result.body.error).toBe('publisher must be string')
                expect(result.status).toBe(400)

            })

            it('Update Year to Non Number Should Return Error', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'year': 'angka123' })

                expect(result.body.error).toBe('year must be number')
                expect(result.status).toBe(400)

            })

            it('Update Page Count to Non Number Should Return Error', async () => {
                const result = await request(app).patch(`/book/${currentId}`).send({ 'pageCount': 'angka123' })

                expect(result.body.error).toBe('pageCount must be number')
                expect(result.status).toBe(400)

            })
        });
    })

    describe('Delete Book By Id', () => {

        beforeAll(async () => {
            const book = await mongo.db.collection('books').insertOne({ ...book1 });
            currentId = book.insertedId.toString()
        })

        afterAll(async () => {
            await mongo.dropCollections();
        })

        describe('Using Existing Id', () => {
            it('should Return Success Response and Deleted Data', async () => {
                const result = await request(app).delete(`/book/${currentId}`)

                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)
                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('name')
                expect(result.body.data).toHaveProperty('author')
                expect(result.body.data).toHaveProperty('year')
                expect(result.body.data).toHaveProperty('summary')
                expect(result.body.data).toHaveProperty('publisher')
                expect(result.body.data).toHaveProperty('pageCount')
                expect(result.body.data).not.toHaveProperty('users')
                expect(result.body.data).toMatchObject(book1)
                expect(result.body.message).toBe(ResponseMessage.SuccessDeleted)
            })
        });
        describe('Using Non Exisiting Id', () => {
            it('Non Exist Id Should Return Error 404', async () => {
                const result = await request(app).delete(`/book/nonExistingId`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)

            })
        })
    })
})