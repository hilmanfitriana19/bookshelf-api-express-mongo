import morgan from "morgan";
import { logger } from "../logger/logger";

const stream = {
    write: (message) => logger.http(message),
};


const morganMiddleware = morgan(
    ":remote-addr :method :url :status :res[content-length] - :response-time ms",
    { stream }
);


export default morganMiddleware;