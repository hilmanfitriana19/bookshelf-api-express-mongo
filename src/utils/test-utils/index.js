import mongoose from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'

let mongoMemoryServer;
export let db;


export const connectDB = async () => {
    mongoMemoryServer = await MongoMemoryServer.create()

    const dbUrl = mongoMemoryServer.getUri()

    await mongoose.connect(dbUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    db = mongoose.connection;
}

export const disconnectDB = async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.connection.close();
    await mongoMemoryServer.stop();
}

export const dropCollections = async () => {
    const collections = await mongoose.connection.db.collections()
    for (const collection of collections) {
        await collection.deleteMany()
    }
};

export default {
    db
}