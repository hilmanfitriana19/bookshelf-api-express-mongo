/* eslint-disable camelcase */
import request from 'supertest'
import app from '../src/app'
import { ResponseMessage } from '../src/utils/constant/response.message.constant'
import * as mongo from '../src/utils/test-utils'
import redisClient from '../src/cache/redis'
import mongoose from 'mongoose'

const offset = 0
const limit = 1

let userBookId = null
let userId_1 = null
let bookId_1 = null

let userId_2 = null
let bookId_2 = null

const nonExistID = {
    userId: '640a0d9dee39ad0d8aefcb1c',
    bookId: '640a0d6fee39ad0d8aefcb16'
}

const userData1 = {
    "name": "user 1",
    "password": "user 1234",
    "email": "user1@info.com"
}

const bookData1 = {
    name: "Coming From tomorrow",
    summary: "Coming from tomorrow to meet you in the past",
    publisher: "Dadang Konelo",
    author: 'Dadang Konelo',
    pageCount: 30,
    year: 2006
}

const userData2 = {

    "name": "user 2",
    "password": "user 1234",
    "email": "user2@info.com"
}

const bookData2 = {
    name: "Book 2",
    author: 'Dadang Konelo',
    pageCount: 30,
    year: 2006
}

let userBookData1, userBookData2

jest.mock('../src/middleware/authentication', () => ({
    JwtAuth: jest.fn((req, res, next) => next()),
}))

beforeAll(async () => {
    await mongo.connectDB()
    const user1 = await mongo.db.collection('users').insertOne({ ...userData1 })
    const book1 = await mongo.db.collection('books').insertOne({ ...bookData1 })
    const user2 = await mongo.db.collection('users').insertOne({ ...userData2 })
    const book2 = await mongo.db.collection('books').insertOne({ ...bookData2 })
    bookId_1 = book1.insertedId.toString()
    userId_1 = user1.insertedId.toString()
    bookId_2 = book2.insertedId.toString()
    userId_2 = user2.insertedId.toString()
    userBookData1 = {
        reader: userId_1, book: bookId_1, readPage: 50
    }
    userBookData2 = {
        reader: userId_2, book: bookId_2, readPage: 14
    }

    jest.spyOn(redisClient, 'get').mockImplementation((name) => { return null })
})

afterAll(async () => {
    await mongo.dropCollections()
    mongo.disconnectDB()
})


describe('User Book API', () => {
    describe('Create User Book', () => {
        describe('Using Correct Request Body', () => {
            it('should return inserted data', async () => {

                const result = await request(app).post('/user-book/')
                    .send({ ...userBookData1 })

                expect(result.status).toBe(201)
                expect(result.body.message).toBe(ResponseMessage.SuccessCreated)
                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toMatchObject(userBookData1)

                const userData = await request(app).get(`/user/${userBookData1.reader}/detail`)
                expect(userData.body.books.length).toBeGreaterThan(0)
                expect(userData.body.books[0].name).toBe(bookData1.name)
                expect(userData.body.books[0].id).toBe(bookId_1)
                userBookId = result.body.data.id

            });
        });

        describe('Using Exist Combination of User and Book', () => {
            it('should return Error', async () => {
                const result = await request(app).post('/user-book/')
                    .send({ ...userBookData1 })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe(ResponseMessage.DuplicateUserBook)
            });
        });

        describe('Using Invalid Request Body', () => {
            it('Non Existing User Data with Reader should return Error', async () => {

                const result = await request(app).post('/user-book/')
                    .send({
                        "reader": 'NonExistUserId',
                        "book": bookId_2,
                        "readPage": 14
                    })

                expect(result.status).toBe(404)
                expect(result.body.error).toBe('Data User Not Found')
            });

            it('Empty Reader should return Error', async () => {
                const result = await request(app).post('/user-book/')
                    .send({
                        "reader": '',
                        "book": bookId_2,
                        "readPage": 25
                    })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('reader cannot be empty')
            });

            it('Non Existing Reader in Request Body should return Error', async () => {
                const result = await request(app).post('/user-book/')
                    .send({
                        "book": bookId_2,
                        "readPage": 25
                    })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('reader is required')
            });

            it('Non Existing Book Data with id should return Error', async () => {
                const result = await request(app).post('/user-book/')
                    .send({
                        "reader": userId_2,
                        "book": 'NonExistBookId',
                        "readPage": 14
                    })

                expect(result.status).toBe(404)
                expect(result.body.error).toBe('Data Book Not Found')
            });

            it('Empty Book should return Error', async () => {
                const result = await request(app).post('/user-book/')
                    .send({
                        "reader": userId_2,
                        "book": '',
                        "readPage": 25
                    })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('book cannot be empty')
            });

            it('Non Existing Book in Request Body should return Error', async () => {
                const result = await request(app).post('/user-book/')
                    .send({
                        "reader": userId_2,
                        "readPage": 25
                    })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('book is required')
            });

            it('Non Number readPage should return Error', async () => {
                const result = await request(app).post('/user-book/')
                    .send({
                        "reader": userId_2,
                        "book": bookId_2,
                        "readPage": 'NonNumber'
                    })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('readPage must be number')
            });
        });
    });

    describe('Get List User Book', () => {

        describe('Without Offset and Limit', () => {

            beforeAll(async () => {
                await mongo.db.collection('userbooks').insertOne({ ...userBookData2 });
            })

            it('should Return List Book', async () => {
                const result = await request(app).get('/user-book/')
                expect(result.status).toBe(200)

                expect(result.body).toBeInstanceOf(Array)
                expect(result.body[0]).toHaveProperty('id')
                expect(result.body[0]).toHaveProperty('reader')
                expect(result.body[0]).toHaveProperty('book')
                expect(result.body[0]).toHaveProperty('readPage')

                expect(result.body[1]).toHaveProperty('id')
                expect(result.body[1]).toHaveProperty('reader')
                expect(result.body[1]).toHaveProperty('book')
                expect(result.body[1]).toHaveProperty('readPage')


                expect(result.body[0]).toMatchObject(userBookData1)

                expect(result.body[1]).toMatchObject(userBookData2)

                expect(result.body.length).toBe(2)
            });
        });

        describe('From Redis Cache', () => {
            afterAll(async () => {
                jest.spyOn(redisClient, 'get').mockImplementation((name) => { return null })
            })


            it('should Return List User Book', async () => {
                const mockRedis = jest.spyOn(redisClient, 'get').mockImplementation((name) => {
                    return JSON.stringify(
                        [
                            userBookData1,
                            userBookData2
                        ]
                    )
                })
                const result = await request(app).get('/user-book/')

                expect(mockRedis).toHaveBeenCalled()
                expect(mockRedis).toBeCalledWith('UserBookList_0_undefined')
                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Array)
                expect(result.body.length).toBe(2)

                expect(result.body[0]).toMatchObject(userBookData1)
                expect(result.body[1]).toMatchObject(userBookData2)

            });
        });

        describe('Using Offset and Limit Data', () => {
            describe('Using Number in Offset and Limit', () => {
                it('Should Return List User Book', async () => {
                    const result = await request(app).get('/user-book/')
                        .query({ offset, limit })

                    expect(result.status).toBe(200)
                    expect(result.body).toBeInstanceOf(Array)
                    expect(result.body.length).toBe(limit)
                    expect(result.body[0]).toMatchObject(userBookData1)
                })
            })

            describe('Using Non Number Offset', () => {
                it('Should Return Error Message', async () => {
                    const result = await request(app).get('/user-book/')
                        .query({ offset: 'lima', limit })

                    expect(result.body.error).toEqual('offset must be number')
                    expect(result.status).toBe(400)
                })
            })
            describe('Using Non Number Limit', () => {
                it('Should Return Error Message', async () => {
                    const result = await request(app).get('/user-book/')
                        .query({ offset, limit: 'lima' })

                    expect(result.body.error).toEqual('limit must be number')
                    expect(result.status).toBe(400)
                })
            })
        });
    });
    describe('Get One User Book By Id', () => {

        describe('Using Existing Id', () => {
            it('should Return User Book Data', async () => {
                const result = await request(app).get(`/user-book/${userBookId}`)

                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Object)

                expect(result.body).toMatchObject(userBookData1)
                expect(result.body).toHaveProperty('id')
                expect(result.body).toHaveProperty('reader')
                expect(result.body).toHaveProperty('book')
                expect(result.body).toHaveProperty('readPage')

            })
        });

        describe('Using Non Existing Id', () => {
            it('Should Return Error 404', async () => {
                const result = await request(app).get(`/user-book/nonExistingId`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)

            })
        });
    });


    describe('Update User Book By Id', () => {

        describe('Using Existing Id', () => {
            it('Update readPage with number should Return Success Response and Updated Data', async () => {
                const result = await request(app).patch(`/user-book/${userBookId}`).send({ 'readPage': 89 })

                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)

                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('reader')
                expect(result.body.data).toHaveProperty('book')
                expect(result.body.data).toHaveProperty('readPage')
                expect(result.body.data.readPage).toBe(89)
                expect(result.body.message).toBe(ResponseMessage.SuccessUpdated)

                // cek data di user, terupdate atau tidak
                const userData = await request(app).get(`/user/${userId_1}/detail`)

                expect(userData.body.books.length).toBeGreaterThan(0)
                expect(userData.body.books[0].name).toBe(bookData1.name)
                expect(userData.body.books[0].id).toBe(bookId_1)
                expect(userData.body.books[0].readPage).toBe(89)

            })

            it('Update Using Existing reader should Return Success Response and Updated Data', async () => {
                const result = await request(app).patch(`/user-book/${userBookId}`).send({ 'reader': userId_2, 'book': bookId_1 })

                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)

                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('reader')
                expect(result.body.data).toHaveProperty('book')
                expect(result.body.data).toHaveProperty('readPage')
                expect(result.body.data.reader).toBe(userId_2)
                expect(result.body.message).toBe(ResponseMessage.SuccessUpdated)

                // cek data di user, terupdate atau tidak
                const userData1 = await request(app).get(`/user/${userId_2}/detail`)
                expect(userData1.body.books.length).toBeGreaterThan(0)
                expect(userData1.body.books[0].name).toBe(bookData1.name)
                expect(userData1.body.books[0].id).toBe(bookId_1)

                const userData2 = await request(app).get(`/user/${userId_1}/detail`)
                expect(userData2.body.books.length).toBe(0)
            })

            it('Update Using Existing book should Return Success Response and Updated Data', async () => {
                const result = await request(app).patch(`/user-book/${userBookId}`).send({ 'book': bookId_2, 'reader': userId_2 })
                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)

                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('reader')
                expect(result.body.data).toHaveProperty('book')
                expect(result.body.data).toHaveProperty('readPage')
                expect(result.body.data.book).toBe(bookId_2)
                expect(result.body.message).toBe(ResponseMessage.SuccessUpdated)

                // cek data di book, terupdate atau tidak, cek di hasil update
                let bookArray = await mongo.db.collection('books').find({ _id: new mongoose.Types.ObjectId(bookId_2) })
                let book = await bookArray.toArray()
                expect(book[0].users[0].toString()).toBe(result.body.data.id)

                // cek data di book awal
                bookArray = await mongo.db.collection('books').find({ _id: new mongoose.Types.ObjectId(bookId_1) })
                book = await bookArray.toArray()
                expect(book[0].users.length).toBe(0)
            })

            it('Update Using Existing book and Existing reader should Return Success Response and Updated Data', async () => {
                const result = await request(app).patch(`/user-book/${userBookId}`).send({ ...userBookData1 })
                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)

                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('reader')
                expect(result.body.data).toHaveProperty('book')
                expect(result.body.data).toHaveProperty('readPage')
                expect(result.body.data.book).toBe(bookId_1)
                expect(result.body.data.reader).toBe(userId_1)
                expect(result.body.message).toBe(ResponseMessage.SuccessUpdated)

                // cek data di book, terupdate atau tidak, cek di hasil update
                let bookArray = await mongo.db.collection('books').find({ _id: new mongoose.Types.ObjectId(bookId_1) })
                let book = await bookArray.toArray()
                expect(book[0].users[0].toString()).toBe(result.body.data.id)

                // cek data di pada data awal
                bookArray = await mongo.db.collection('books').find({ _id: new mongoose.Types.ObjectId(bookId_2) })
                book = await bookArray.toArray()
                expect(book[0].users.length).toBe(0)

                // cek data di user, terupdate atau tidak, cek di hasil update
                const userData1 = await request(app).get(`/user/${userId_1}/detail`)
                expect(userData1.body.books.length).toBeGreaterThan(0)
                expect(userData1.body.books[0].name).toBe(bookData1.name)
                expect(userData1.body.books[0].id).toBe(bookId_1)

                // cek di data awal
                const userData2 = await request(app).get(`/user/${userId_2}/detail`)
                expect(userData2.body.books.length).toBe(0)
            })

            it('Update Using Non Existing Book Should Return Error 404', async () => {
                const result = await request(app).patch(`/user-book/${userBookId}`).send({ 'book': nonExistID.bookId })
                expect(result.status).toBe(404)

                expect(result.body.error).toBe('Data Book Not Found')
            })

            it('Update Using Non Existing User Should Return Error 404', async () => {
                const result = await request(app).patch(`/user-book/${userBookId}`).send({ 'reader': nonExistID.userId })
                expect(result.status).toBe(404)

                expect(result.body.error).toBe('Data User Not Found')
            })

            it('Update Using Non Number readPage should Return Error', async () => {
                const result = await request(app).patch(`/user-book/${userBookId}`).send({ 'readPage': 'limaRatus' })
                expect(result.status).toBe(400)

                expect(result.body.error).toBe('readPage must be number')
            })
        });

        describe('Using NonExisting Id', () => {
            it('Should Return Error 404', async () => {
                const result = await request(app).patch(`/user-book/NonExistUserBookId`).send({ 'readPage': 100 })
                expect(result.status).toBe(404)

                expect(result.body.error).toBe(ResponseMessage.DataNotFound)
            });
        });
    })

    describe('Delete User Book By Id', () => {

        describe('Using Existing Id', () => {
            it('should Return Success Response and Deleted Data', async () => {
                const result = await request(app).delete(`/user-book/${userBookId}`)

                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)
                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('reader')
                expect(result.body.data).toHaveProperty('book')
                expect(result.body.data).toHaveProperty('readPage')
                expect(result.body.data).toMatchObject(userBookData1)
                expect(result.body.message).toBe(ResponseMessage.SuccessDeleted)
            })
        });
        describe('Using Non Exisiting Id', () => {
            it('Non Exist Id Should Return Error 404', async () => {
                const result = await request(app).delete(`/user-book/nonExistingId`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)

            })
        })
    })
});