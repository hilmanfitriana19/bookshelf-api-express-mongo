import { JwtAuth } from "../src/middleware/authentication";
import * as mongo from '../src/utils/test-utils'
import jwt from 'jsonwebtoken'
import env from "../src/config/env";
import { ResponseMessage } from "../src/utils/constant/response.message.constant";

const userData1 = {
    "name": "user 1",
    "password": "user 1234",
    "email": "user1@info.com"
}

const user = {}

const res = {
    status: jest.fn(() => res),
    json: jest.fn(),
    send: jest.fn()
};

const next = jest.fn();

beforeAll(async () => {

    await mongo.connectDB()

    const userInsert = await mongo.db.collection('users').insertOne(userData1)
    user.id = userInsert.insertedId.toString()
})


afterAll(async () => {
    mongo.dropCollections()
    mongo.disconnectDB()
})

describe('Auth Middleware', () => {

    it('Valid Token Should Allow Access', async () => {
        const payload = { user_id: user.id, time: Date.now() };
        const token = jwt.sign(payload, env.JWT_ACCESS_KEY, { expiresIn: env.JWT_ACCESS_EXPIRATION_TIME })
        const req = {
            cookies: {
                Authentication: token
            }
        };

        const next = jest.fn();

        await JwtAuth(req, res, next);

        expect(req.user_id).toEqual(user.id);
        expect(next).toHaveBeenCalled();
    });

    it('No Token Provided should return Error 401', async () => {
        const req = {
            cookies: {}
        };

        await JwtAuth(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ error: ResponseMessage.NoToken });
        expect(next).not.toHaveBeenCalled();
    });

    it('Not Defined User should return error 401', async () => {

        const payload = { user_id: 'NotDefinedId', time: Date.now() };
        const token = jwt.sign(payload, env.JWT_ACCESS_KEY, { expiresIn: env.JWT_ACCESS_EXPIRATION_TIME })

        const req = {
            cookies: {
                Authentication: token
            }
        };

        await JwtAuth(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ error: ResponseMessage.InvalidToken });
        expect(next).not.toHaveBeenCalled();
    });


    it('Invalid Token Should return Error 401', async () => {

        const req = {
            cookies: {
                Authentication: `Invalid Token. Token Salah`
            }
        };

        await JwtAuth(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ error: ResponseMessage.InvalidToken });
        expect(next).not.toHaveBeenCalled();
    });

    it('Expired Token Should return Error 401', async () => {
        const payload = { user_id: user.id, time: Date.now() };
        const token = jwt.sign(payload, env.JWT_ACCESS_KEY, { expiresIn: -2000 })


        const req = {
            cookies: {
                Authentication: token
            }
        };

        await JwtAuth(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ error: ResponseMessage.TokenExpired });
        expect(next).not.toHaveBeenCalled();
    });



})