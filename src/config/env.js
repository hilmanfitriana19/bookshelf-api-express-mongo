import dotenv from 'dotenv';

dotenv.config();

export default {
    PORT: process.env.PORT,

    DATABASE_TYPE: 'mongodb',
    DATABASE_HOST: process.env.DB_HOST,
    DATABASE_PORT: process.env.DB_PORT,
    DATABASE_NAME: process.env.DB_NAME,
    DATABASE_USERNAME: process.env.DB_USERNAME,
    DATABASE_PASSWORD: process.env.DB_PASSWORD,

    JWT_ACCESS_KEY: process.env.JWT_ACCESS_TOKEN_KEY,
    JWT_ACCESS_EXPIRATION_TIME: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME,

    JWT_REFRESH_KEY: process.env.JWT_REFRESH_TOKEN_KEY,
    JWT_REFRESH_EXPIRATION_TIME: process.env.JWT_REFRESH_TOKEN_EXPIRATION_TIME,

    REDIS_HOST: process.env.REDIS_HOST,
    REDIS_PORT: process.env.REDIS_PORT,
    REDIS_EXPIRE: process.env.REDIS_EXPIRE,

    LOGGER_PATH: process.env.LOGGER_PATH,

    KAFKA_CLIENT_ID: process.env.KAFKA_CLIENT_ID,
    KAFKA_HOST: process.env.KAFKA_HOST,
    KAFKA_PORT: process.env.KAFKA_PORT


}