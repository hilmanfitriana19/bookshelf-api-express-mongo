import request from 'supertest'
import app from '../src/app'
import { ResponseMessage } from '../src/utils/constant/response.message.constant'
import * as mongo from '../src/utils/test-utils'
import jwt from 'jsonwebtoken'
import env from '../src/config/env'
import { getMaxAge } from '../src/utils/helper/cookie.helper'


const userData1 = {
    "name": "user 1",
    "password": "user 1234",
    "email": "user1@info.com"
}

const user = {
    password: Buffer.from(userData1.password, 'utf-8').toString('base64'),
    id: null
}

let cookie = null;

jest.mock('../src/middleware/authentication', () => ({
    JwtAuth: jest.fn((req, res, next) => next()),
}))

beforeAll(async () => {
    await mongo.connectDB()
    const userData = await request(app).post('/user/').send(userData1)
    user.id = userData.body.data.id
    jest.clearAllMocks()
})

afterAll(async () => {
    mongo.dropCollections()
    mongo.disconnectDB()
})


describe('Auth API', () => {
    describe('Login', () => {
        describe('Using Correct Email and Password', () => {
            it('shoud return true', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: userData1.email,
                    password: user.password
                })

                expect(result.status).toBe(200)
                expect(result.body.message).toBe(ResponseMessage.LoginSucess)
                expect(result.header['set-cookie']).toBeTruthy()
                const cookieHeaders = result.header['set-cookie']
                expect(cookieHeaders[0].includes('Authentication')).toBeTruthy()
                expect(cookieHeaders[1].includes('Refresh')).toBeTruthy()
                // check auth token
                const authToken = cookieHeaders[0].split(';')[0].split('=')[1]
                let payload = jwt.verify(authToken, env.JWT_ACCESS_KEY)
                expect(payload.user_id).toBe(user.id)
                // check refresh token
                const refreshToken = cookieHeaders[1].split(';')[0].split('=')[1]
                payload = jwt.verify(refreshToken, env.JWT_REFRESH_KEY)
                expect(payload.user_id).toBe(user.id)
                cookie = cookieHeaders

            })
        });
        describe('Using Invalid Request Body', () => {
            it('Non Existing Email must Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    password: user.password
                })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('email is required')
            });

            it('Non Email Structure must Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: 'testWrongStructure Email',
                    password: user.password
                })
                expect(result.body.error).toBe('email is invalid')
                expect(result.status).toBe(400)
            })
            it('Empty String email must Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: 123456789,
                    password: user.password
                })
                expect(result.body.error).toBe('email must be string')
                expect(result.status).toBe(400)
            })
            it('Empty String email must Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: '',
                    password: user.password
                })
                expect(result.body.error).toBe('email cannot be an empty')
                expect(result.status).toBe(400)
            })
            it('Non Exist Password must Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: userData1.email,
                })
                expect(result.body.error).toBe('password is required')
                expect(result.status).toBe(400)
            })
            it('Non String Password must Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: userData1.email,
                    password: 123456
                })
                expect(result.body.error).toBe('password must be string')
                expect(result.status).toBe(400)
            })
            it('Empty String Password Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: userData1.email,
                    password: ''
                })
                expect(result.body.error).toBe('password cannot be an empty')
                expect(result.status).toBe(400)
            })

            it('Wrong Email Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: 'test@email.com',
                    password: user.password
                })
                expect(result.body.error).toBe(ResponseMessage.LoginFailed)
                expect(result.status).toBe(401)
            })

            it('Wrong Password Return Error', async () => {
                const result = await request(app).post('/auth/login').send({
                    email: userData1.email,
                    password: 'wrong password'
                })
                expect(result.body.error).toBe(ResponseMessage.LoginFailed)
                expect(result.status).toBe(401)
            })

        });
    });
    describe('Refresh Auth Token', () => {
        describe('Using Correct Flow', () => {
            it('should return Success Message', async () => {
                const result = await request(app).post('/auth/refresh')
                    .set('Cookie', cookie)

                expect(result.status).toBe(200)
                expect(result.body.message).toBe(ResponseMessage.SuccessRefreshToken)
                expect(result.header['set-cookie']).toBeTruthy()

                const cookieHeaders = result.header['set-cookie']
                expect(cookieHeaders[0].includes('Authentication')).toBeTruthy()

                // check bahwa token berbeda dengan token sebelumnya
                expect(cookie[0]).not.toEqual(cookieHeaders[0])
                // check auth token dari id yang sama
                const authToken = cookieHeaders[0].split(';')[0].split('=')[1]
                const payload = jwt.verify(authToken, env.JWT_ACCESS_KEY)
                expect(payload.user_id).toBe(user.id)
            });

            it('Using Undefined user_id should return Error 401 ', async () => {
                const tempRefreshToken = jwt.sign({ user_id: 'NotDefinedUserId' }, env.JWT_REFRESH_KEY, { expiresIn: env.JWT_REFRESH_EXPIRATION_TIME })
                const refreshTokenCookie = `Refresh=${tempRefreshToken}; HttpOnly; Path=/; Max-Age=${getMaxAge(env.JWT_REFRESH_EXPIRATION_TIME)}`;

                const result = await request(app).post('/auth/refresh')
                    .set('Cookie', [refreshTokenCookie])

                expect(result.status).toBe(401)
                expect(result.body.error).toBe(ResponseMessage.InvalidToken)
                expect(result.header['set-cookie']).toBeFalsy()

            });

            it('Using different refresh token should return Error 401 ', async () => {
                const result = await request(app).post('/auth/refresh')
                    .set('Cookie', [`Refresh=ejURstyu${cookie[1].substr(15)}`])

                expect(result.status).toBe(401)
                expect(result.body.error).toBe(ResponseMessage.InvalidToken)
                expect(result.header['set-cookie']).toBeFalsy()

            });

            it('Using different refresh token saved in database should return Error 401 ', async () => {
                const tempRefreshToken = jwt.sign({ user_id: user.id, time: Date.now() }, env.JWT_REFRESH_KEY, { expiresIn: env.JWT_REFRESH_EXPIRATION_TIME })
                const refreshTokenCookie = `Refresh=${tempRefreshToken}; HttpOnly; Path=/; Max-Age=${env.JWT_REFRESH_EXPIRATION_TIME}`;

                const result = await request(app).post('/auth/refresh')
                    .set('Cookie', [refreshTokenCookie])

                expect(result.status).toBe(401)
                expect(result.body.error).toBe(ResponseMessage.InvalidToken)
                expect(result.header['set-cookie']).toBeFalsy()

            });

            it('Using Expired refresh token should return Error 401 ', async () => {
                // generate expired token

                const tempRefreshToken = jwt.sign({ user_id: user.id, time: Date.now() }, env.JWT_REFRESH_KEY, { expiresIn: -2000 })
                const refreshTokenCookie = `Refresh=${tempRefreshToken}; HttpOnly; Path=/; Max-Age=${env.JWT_REFRESH_EXPIRATION_TIME}`;

                const result = await request(app).post('/auth/refresh')
                    .set('Cookie', [refreshTokenCookie])

                expect(result.status).toBe(401)
                expect(result.body.error).toBe(ResponseMessage.TokenExpired)
                expect(result.header['set-cookie']).toBeFalsy()

            });

        });

        describe('Using Invalid Data', () => {
            it('No Refresh Token Should return Error 401', async () => {

                const result = await request(app).post('/auth/refresh')

                expect(result.status).toBe(401)
                expect(result.body.error).toBe(ResponseMessage.NoToken)
                expect(result.header['set-cookie']).toBeFalsy()
            });
        });
    });

    describe('Logout ', () => {
        describe('With Refresh Token In Request', () => {
            it('should return success message', async () => {
                const result = await request(app).post('/auth/logout')
                    .set('Cookie', cookie)

                expect(result.status).toBe(200)
                expect(result.body.message).toBe(ResponseMessage.LogoutSuccess)
                const cookieHeaders = result.header['set-cookie']
                const accessTokenCookie = `Authentication=; HttpOnly; Path=/; Max-Age=0`;
                const refreshTokenCookie = `Refresh=; HttpOnly; Path=/; Max-Age=0`;

                expect(cookieHeaders[0]).toBe(accessTokenCookie)
                expect(cookieHeaders[1]).toBe(refreshTokenCookie)

            });
        });
        describe('No Refresh Token', () => {
            it('should return success message', async () => {

                const result = await request(app).post('/auth/logout')

                expect(result.status).toBe(200)
                expect(result.body.message).toBe(ResponseMessage.LogoutSuccess)
                const cookieHeaders = result.header['set-cookie']
                const accessTokenCookie = `Authentication=; HttpOnly; Path=/; Max-Age=0`;
                const refreshTokenCookie = `Refresh=; HttpOnly; Path=/; Max-Age=0`;

                expect(cookieHeaders[0]).toBe(accessTokenCookie)
                expect(cookieHeaders[1]).toBe(refreshTokenCookie)
            });
        });

        describe('Using Invalid Data', () => {
            it('Using Expired refresh token should return Error 400 ', async () => {
                // generate expired token

                const tempRefreshToken = jwt.sign({ user_id: user.id, time: Date.now() }, env.JWT_REFRESH_KEY, { expiresIn: -2000 })
                const refreshTokenCookie = `Refresh=${tempRefreshToken}; HttpOnly; Path=/; Max-Age=${env.JWT_REFRESH_EXPIRATION_TIME}`;

                const result = await request(app).post('/auth/logout')
                    .set('Cookie', [refreshTokenCookie])

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('Failed to Logout')
                expect(result.header['set-cookie']).toBeFalsy()
            });

            it('Using Undefined user_id should return Error 401 ', async () => {
                const tempRefreshToken = jwt.sign({ user_id: 'NotDefinedUserId' }, env.JWT_REFRESH_KEY, { expiresIn: env.JWT_REFRESH_EXPIRATION_TIME })
                const refreshTokenCookie = `Refresh=${tempRefreshToken}; HttpOnly; Path=/; Max-Age=${getMaxAge(env.JWT_REFRESH_EXPIRATION_TIME)}`;

                const result = await request(app).post('/auth/logout')
                    .set('Cookie', [refreshTokenCookie])

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('Failed to Logout')
                expect(result.header['set-cookie']).toBeFalsy()

            });

            it('Using different refresh token should return Error 401 ', async () => {
                const result = await request(app).post('/auth/logout')
                    .set('Cookie', [`Refresh=ejURstyu${cookie[1].substr(15)}`])

                expect(result.status).toBe(400)
                expect(result.body.error).toBe('Failed to Logout')
                expect(result.header['set-cookie']).toBeFalsy()

            });

            it('Using different refresh token saved in database should return Error 401 ', async () => {
                const tempRefreshToken = jwt.sign({ user_id: user.id, time: Date.now() }, env.JWT_REFRESH_KEY, { expiresIn: env.JWT_REFRESH_EXPIRATION_TIME })
                const refreshTokenCookie = `Refresh=${tempRefreshToken}; HttpOnly; Path=/; Max-Age=${env.JWT_REFRESH_EXPIRATION_TIME}`;

                const result = await request(app).post('/auth/logout')
                    .set('Cookie', [refreshTokenCookie])

                expect(result.status).toBe(200)
                expect(result.body.message).toBe(ResponseMessage.LogoutSuccess)
                expect(result.header['set-cookie']).toBeTruthy()

            });
        });
    });


});