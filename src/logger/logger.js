/* eslint-disable new-cap */
import winston from "winston";
import winstonDailyRotateFile from 'winston-daily-rotate-file';
import env from "../config/env";

export const logger = (process.env.NODE_ENV === 'test ') ?
    {
        error: () => { },
        info: () => { },
        http: () => { },
    }
    : winston.createLogger({
        exitOnError: false,
        level: 'http',
        transports: [
            new winston.transports.Console(),
            new winstonDailyRotateFile({
                dirname: env.LOGGER_PATH + '/app/combine',
                filename: 'combine-%DATE%',
                extension: '.log',
                level: 'info',
                zippedArchive: true,
                handleExceptions: true,
                datePattern: 'YY-MM-DD',
                maxSize: '20m',
                maxFiles: '14d',

            }),
            new winstonDailyRotateFile({
                dirname: env.LOGGER_PATH + '/app/error',
                filename: 'error-%DATE%',
                extension: '.log',
                level: 'error',
                zippedArchive: true,
                datePattern: 'YY-MM-DD',
                maxSize: '20m',
                maxFiles: '14d'
            }),
        ],
        format: winston.format.combine(
            winston.format.timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }),
            winston.format.printf(info => `[${info.timestamp}] - [${info.level}] - ${info.message}`)
        ),
    });