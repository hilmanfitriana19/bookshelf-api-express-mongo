import bcrypt from 'bcrypt';

export async function encodePassword(password) {
    const salt = await bcrypt.genSalt();
    return await bcrypt.hash(password, salt);
}

export function isCorrectPassword(rawPassword, hash) {
    return bcrypt.compareSync(rawPassword, hash);
}