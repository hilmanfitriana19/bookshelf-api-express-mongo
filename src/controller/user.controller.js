import mongoose from "mongoose";
import redisClient from "../cache/redis";
import { logger } from "../logger/logger";
import userService from "../service/user.service";
import { MongoErrorCode } from "../utils/constant/mongodb.error";
import { ResponseMessage } from "../utils/constant/response.message.constant";
import { objectToStr } from "../utils/log.utils";
import { responseUtil } from "../utils/response/response.utils";
import { queryValidator } from "../utils/validator/query.validator";
import { userValidator } from "../utils/validator/user-validator";


const userController = ({
    create: async (req, res) => {
        const body = req.body;
        logger.info(`Request Create Data User with body ${objectToStr(body)}`)

        const validatorResult = userValidator.create.validate(body, { allowUnknown: true })

        if (validatorResult.error) {
            logger.error(`Request Create User Data Failed ${validatorResult.error.message}`)
            return responseUtil.FailProcess(res, validatorResult.error.message);
        }

        try {
            const data = await userService.create(body)
            logger.info('Successfully create user data')
            delete data._doc.books
            return responseUtil.SuccessCreated(res, data)
        } catch (e) {
            logger.error(`Request Create New User Failed : ${e?.message || e}`)
            if (e.code === MongoErrorCode.DUPLICATE)
                return responseUtil.FailProcess(res, ResponseMessage.EmailTaken)
            return responseUtil.InternalServerError(res)
        }
    },

    getAll: async (req, res) => {
        logger.info('Request Get List User Data')

        const offset = req.query.offset;
        const limit = req.query.limit;

        const validationResult = queryValidator.validate({ offset, limit }, { allowUnknown: true })
        if (validationResult.error) {
            logger.error(`Request Create User Data Failed : ${validationResult.error.message}`)
            return responseUtil.FailProcess(res, validationResult.error.message);
        }

        const cacheResult = await redisClient.get(`UserList_${offset || 0}_${limit}`)

        if (cacheResult) {
            logger.info(`Set response from redis Cache`)
            return res.send(JSON.parse(cacheResult))
        } else {
            logger.info(`Set Response From Database`)
            const result = await userService.getAll(offset, limit)
            redisClient.setData(`UserList_${offset || 0}_${limit}`, JSON.stringify(result))
            return res.send(result);
        }
    },

    getAllDetail: async (req, res) => {
        logger.info(`Request Get List User Data Detail`);

        const offset = req.query.offset;
        const limit = req.query.limit;

        const validationResult = queryValidator.validate({ offset, limit }, { allowUnknown: true })
        if (validationResult.error) {
            logger.error(`Request Create User Data Failed : ${validationResult.error.message}`);
            return responseUtil.FailProcess(res, validationResult.error.message);
        }

        const cacheResult = await redisClient.get(`UserListDetail_${offset || 0}_${limit}`)
        if (cacheResult) {
            logger.info(`Set response from redis Cache`)
            return res.send(JSON.parse(cacheResult))
        } else {
            logger.info(`Set Response From Database`)
            const result = await userService.getAllDetail(offset, limit)
            redisClient.setData(`UserListDetail_${offset || 0}_${limit}`, JSON.stringify(result))
            return res.send(result);
        }
    },

    getOne: async (req, res) => {
        const { id } = req.params;

        logger.info(`Request Get User Data with id : ${id}`)
        try {
            const result = await userService.getOneById(id)
            return res.send(result);
        } catch (e) {
            logger.error(` Failed to get User Data with id ${id} : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    },

    getOneDetail: async (req, res) => {
        const { id } = req.params;
        logger.info(`Request Get User Data with id : ${id}`)
        try {
            const result = await userService.getOneByIdDetail(id);
            return res.send(result);
        } catch (e) {
            logger.error(` Failed to get User Data with id ${id} : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    },

    update: async (req, res) => {
        const { id } = req.params;
        const body = req.body;

        logger.info(`Request to Update Data User with id : ${id}`)

        const validatorResult = userValidator.update.validate(body, { allowUnknown: true })

        if (validatorResult.error) {
            logger.error(`Request Update User Data Failed ${validatorResult.error.message}`)
            return responseUtil.FailProcess(res, validatorResult.error.message);
        }
        try {

            await userService.getOneById(id);

            const data = await userService.update(id, body)

            logger.info(`Successfully Update User Data with id : ${id}`)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessUpdated, data)

        } catch (e) {
            logger.error(`Failed to Update Data User with id ${id} : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)

            if (e.code === MongoErrorCode.DUPLICATE)
                return responseUtil.FailProcess(res, ResponseMessage.EmailTaken)

            return responseUtil.InternalServerError(res)
        }
    },


    delete: async (req, res) => {
        const { id } = req.params;

        logger.info(`Request to Delete User with id : ${id}`)

        try {
            await userService.getOneById(id);

            const data = await userService.delete(id);

            logger.info(`Successfully delete user data with id : ${id}`)
            return responseUtil.SuccessProcess(res, ResponseMessage.SuccessDeleted, data)

        } catch (e) {
            logger.error(`Failed to Delelete User data with id ${id} : ${e?.message || e}`)
            if (e instanceof mongoose.Error.DocumentNotFoundError)
                return responseUtil.NotFoundData(res)
            return responseUtil.InternalServerError(res)
        }
    }
})


export default userController;