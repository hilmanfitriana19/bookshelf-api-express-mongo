import dbConfig from './db.config'
import mongoose from 'mongoose'
import env from '../config/env';

export const connectDB = async () => {
    mongoose.connect(dbConfig.url,
        {
            user: env.DATABASE_USERNAME,
            pass: env.DATABASE_PASSWORD
        }).then(() => {
            console.log('Connect to MongoDB Success');
        }).catch((e) => {
            console.log('Connect to MongoDB Failed ', e)
        })
}

export const disconnectDB = () => {
    mongoose.disconnect()
}