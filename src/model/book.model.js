import mongoose, { Schema } from "mongoose"

const BookSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    author: {
        type: String,
        default: ''
    },
    year: {
        type: Number,
        default: 0
    },
    summary: {
        type: String,
        default: ''
    },
    publisher: {
        type: String,
        default: ''
    },
    pageCount: {
        type: Number,
        default: 0
    },
    users: [{
        type: Schema.Types.ObjectId,
        ref: "UserBook"
    }]
},
    { timestamps: true }
)


BookSchema.method('toJSON', function () {
    const { __v, _id, createdAt, updatedAt, users, ...object } = this.toObject();
    object.id = _id;
    return object;
})

export const Book = mongoose.model('Book', BookSchema);