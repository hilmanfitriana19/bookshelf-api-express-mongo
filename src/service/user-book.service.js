import { Book } from "../model/book.model";
import { UserBook } from "../model/user-book.model";
import { User } from "../model/user.model";

const userBookService = ({

    create: async (body) => {
        const userBook = await new UserBook(body);
        const result = await userBook.save();
        await Book.findByIdAndUpdate(userBook.book, { $push: { users: userBook._id } })
        await User.findByIdAndUpdate(userBook.reader, { $push: { books: userBook._id } })
        return result
    },

    getAll: (offset, limit) => {
        return UserBook.find().skip(offset || 0).limit(limit || null);
    },

    getOneById: (id) => {
        return UserBook.findById(id).orFail();
    },

    update: async (id, body) => {
        const userBook = await UserBook.findById({ _id: id });

        if ('reader' in body) {
            if (userBook.reader.toString() !== body.reader) {
                await User.findByIdAndUpdate(userBook.reader, { $pull: { books: userBook._id } })
                await User.findByIdAndUpdate(body.reader, { $push: { books: userBook._id } })
            }
        }

        if ('book' in body) {
            if (userBook.book.toString() !== body.book) {
                await Book.findByIdAndUpdate(userBook.book, { $pull: { users: userBook._id } })
                await Book.findByIdAndUpdate(body.book, { $push: { users: userBook._id } })
            }
        }
        return UserBook.findOneAndUpdate({ _id: id }, body, { returnOriginal: false })
    },

    delete: async (id) => {
        const userBook = await UserBook.findOneAndDelete({ _id: id });
        await Book.findByIdAndUpdate(userBook.book, { $pull: { users: userBook._id } })
        await User.findByIdAndUpdate(userBook.reader, { $pull: { books: userBook._id } })
        return userBook
    }

})

export default userBookService;