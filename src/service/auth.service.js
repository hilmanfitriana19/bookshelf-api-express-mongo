import { isCorrectPassword } from "../utils/helper/bcrypt.helper";
import userService from "./user.service";
import jwt from 'jsonwebtoken';
import env from "../config/env";
import { getMaxAge } from "../utils/helper/cookie.helper";
import { logger } from "../logger/logger";

const authService = ({

    login: async (data) => {
        const user = await userService.getOneByEmail(data.email);
        const password = Buffer.from(data.password, 'base64').toString('utf-8')
        if (!user || !isCorrectPassword(password, user.password)) {
            return null;
        }
        const payload = { user_id: user.id, time: Date.now() };
        const token = jwt.sign(payload, env.JWT_ACCESS_KEY, { expiresIn: env.JWT_ACCESS_EXPIRATION_TIME })
        const refreshToken = jwt.sign(payload, env.JWT_REFRESH_KEY, { expiresIn: env.JWT_REFRESH_EXPIRATION_TIME })

        userService.update(user.id, { currentRefreshToken: refreshToken });

        const accessTokenCookie = `Authentication=${token}; HttpOnly; Path=/; Max-Age=${getMaxAge(env.JWT_ACCESS_EXPIRATION_TIME)}`;
        const refreshTokenCookie = `Refresh=${refreshToken}; HttpOnly; Path=/;  Max-Age=${getMaxAge(env.JWT_REFRESH_EXPIRATION_TIME)}`;

        return {
            accessTokenCookie,
            refreshTokenCookie
        }
    },

    refresh: async (refreshToken) => {

        const data = jwt.verify(refreshToken, env.JWT_REFRESH_KEY)
        const user = await userService.getOneById(data.user_id)

        if (refreshToken !== user.currentRefreshToken) {
            return null;
        }
        logger.info(`Generate new Access Token for ${user.email} `)

        const payload = { user_id: user.id, time: Date.now() };
        const token = jwt.sign(payload, env.JWT_ACCESS_KEY, { expiresIn: env.JWT_ACCESS_EXPIRATION_TIME })
        return `Authentication=${token}; HttpOnly; Path=/; Max-Age=${getMaxAge(env.JWT_ACCESS_EXPIRATION_TIME)}`;
    },

    logout: async (refreshToken) => {
        if (refreshToken) {
            const data = jwt.verify(refreshToken, env.JWT_REFRESH_KEY)
            const user = await userService.getOneById(data?.user_id)
            if (user && refreshToken === user.currentRefreshToken) {
                userService.update(user.id, { currentRefreshToken: null });
            }
        }
        const accessTokenCookie = `Authentication=; HttpOnly; Path=/; Max-Age=0`;
        const refreshTokenCookie = `Refresh=; HttpOnly; Path=/; Max-Age=0`;

        return {
            accessTokenCookie, refreshTokenCookie
        }
    }


})

export default authService;