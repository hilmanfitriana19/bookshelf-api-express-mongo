export const ResponseMessage = {
    SuccessCreated: 'Data has been Created',
    SuccessUpdated: 'Data has been Updated',
    SuccessDeleted: 'Data has been Deleted',

    FailCreated: 'Failed to Save Data',
    FailUpdated: 'Failed to Update Data',
    FailDeleted: 'Failed to Delete Data',
    NameBookTaken: 'Failed to Save Data, Book Name Already Register',
    UsernameTaken: 'Failed to Save Data, Username Already Taken',
    EmailTaken: 'Failed to Save Data, Email Already Taken',
    DataNotFound: 'Data Not Found',

    LoginFailed: 'Username or Password Not Match',
    LoginSucess: 'Successfully Login',

    TokenExpired: 'Token Expired, Please Login Again',
    InvalidToken: 'Invalid Token, Please Login Again',
    NoToken: 'No Token, Please Login',
    SuccessRefreshToken: 'Successfully Refresh Access Token',
    LogoutSuccess: 'Successfully Logout',

    FailProcess: 'Failed to Process Request',

    DuplicateUserBook: 'Data with combination of bookId and user already exists',

    InternalServerError: 'Something went wrong in server, Internal Server Error'
}