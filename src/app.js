import cookieParser from "cookie-parser";
import express from "express";
import routes from './route/routes';
import morganMiddleware from './middleware/morgan.middleware';
import SwaggerUi from "swagger-ui-express";
const swaggerDocument = require('../openapi.json');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(morganMiddleware);

app.use("/api-docs",SwaggerUi.serve, SwaggerUi.setup(swaggerDocument));


app.use('/',(req,res)=>{
  res.send({message:'Endpoint Bookshelf API Running'})
})



app.use(express.json());
app.use(routes);

export default app;