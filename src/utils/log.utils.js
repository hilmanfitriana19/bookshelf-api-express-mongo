const maskingKey = ['email', 'password']


export function maskingString(data) {

    const emailStructure = data.split('@')
    if (emailStructure.length > 1) {
        return emailStructure[0].substr(0, 2) + '*'.repeat(emailStructure[0].length > 8 ? emailStructure[0].length : 8) + emailStructure[0].substr(-2) + '@' + emailStructure[1]
    } else {
        return '*'.repeat(10)
    }
}

export function objectToStr(obj) {
    const newObj = { ...obj }
    maskingKey.forEach(data => {

        if (data in obj) {
            newObj[data] = maskingString(obj[data])
        }
    })
    return JSON.stringify(newObj);

}