import { User } from "../model/user.model";
import { encodePassword } from "../utils/helper/bcrypt.helper";

const userService = ({

    create: async (body) => {
        const newBody = { ...body }
        newBody.password = await encodePassword(body.password)
        const user = new User(newBody);
        return user.save();
    },

    getAll: (offset, limit) => {
        return User.find().select('-books').skip(offset || 0).limit(limit || null);
    },

    getAllDetail: (offset, limit) => {
        return User.find().populate({
            path: 'books',
            select: ['readPage'],
            populate: {
                path: 'book',
                select: ['_id', 'name'],
            },
            transform: function (data) {
                return {
                    id: data.book.id,
                    name: data.book.name,
                    readPage: data.readPage
                }
            }
        }).skip(offset || 0).limit(limit || null);
    },

    getOneById: (id) => {
        return User.findById(id).select('-books').orFail();
    },

    getOneByIdDetail: (id) => {
        return User.findById(id).populate({
            path: 'books',
            select: ['readPage'],
            populate: {
                path: 'book',
                select: ['_id', 'name'],
            },
            transform: function (data) {
                return {
                    id: data.book.id,
                    name: data.book.name,
                    readPage: data.readPage
                }
            }

        }).orFail();
    },

    update: async (id, body) => {
        if (body?.password)
            body.password = await encodePassword(body?.password)
        return User.findOneAndUpdate({ _id: id }, body, { returnOriginal: false, select: '-books' });
    },

    delete: (id) => {
        return User.findByIdAndDelete({ _id: id }, { returnOriginal: false, select: '-books' });
    },

    getOneByEmail: (email) => {
        return User.findOne({ email });
    }
})

export default userService;