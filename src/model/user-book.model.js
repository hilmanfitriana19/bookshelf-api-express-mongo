import mongoose, { Schema } from "mongoose"

const UserBookSchema = new mongoose.Schema({
    readPage: {
        type: Number,
        default: 0
    },
    reader: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    book: {
        type: Schema.Types.ObjectId,
        ref: "Book"
    },
},
    { timestamps: true }
)

UserBookSchema.index({ reader: 1, book: 1 }, { unique: true })

UserBookSchema.method('toJSON', function () {
    const { __v, _id, createdAt, updatedAt, ...object } = this.toObject();
    object.id = _id;
    return object;
})

export const UserBook = mongoose.model('UserBook', UserBookSchema);