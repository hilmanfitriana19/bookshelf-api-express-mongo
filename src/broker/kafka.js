import { Kafka } from "kafkajs";
import env from "../config/env";

const broker = [`${env.KAFKA_HOST}:${env.KAFKA_PORT}`]

const kafka = new Kafka({
    clientId: env.KAFKA_CLIENT_ID,
    brokers: broker
})


export let producer

(async () => {
    producer = kafka.producer()
    await producer.connect()

    producer.sendData = async function (topic, message) {
        await this.send({
            topic,
            messages: [
                {
                    value: message
                }
            ]
        })
    }

})()