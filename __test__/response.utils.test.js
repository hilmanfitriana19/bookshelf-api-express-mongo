import { ResponseMessage } from "../src/utils/constant/response.message.constant";
import { responseUtil } from "../src/utils/response/response.utils";

const res = {
    status: jest.fn(() => res),
    json: jest.fn(),
    send: jest.fn()
};

const data = {}

describe('Response Utils', () => {
    describe('Success Created', () => {
        it('should return success message with 201', () => {
            responseUtil.SuccessCreated(res, data)

            expect(res.status).toHaveBeenCalledWith(201)
            expect(res.json).toHaveBeenCalledWith({ 'message': ResponseMessage.SuccessCreated, data })
        });
    });
    describe('Success Process', () => {
        it('with non empty message should return success message with 200', () => {
            responseUtil.SuccessProcess(res, 'message', data)

            expect(res.status).toHaveBeenCalledWith(200)
            expect(res.json).toHaveBeenCalledWith({ message: 'message', data })
        });

        it('with null message should return default message with 200', () => {
            responseUtil.SuccessProcess(res, null, data)

            expect(res.status).toHaveBeenCalledWith(200)
            expect(res.json).toHaveBeenCalledWith({ message: ResponseMessage.SuccessProcess, data })
        });

        it('with empty message should return default message with 200', () => {
            responseUtil.SuccessProcess(res, "", data)

            expect(res.status).toHaveBeenCalledWith(200)
            expect(res.json).toHaveBeenCalledWith({ message: undefined, data })
        });

        describe('Not Found Data', () => {
            it('with message should return Not Found Error', () => {
                responseUtil.NotFoundData(res, 'message')

                expect(res.status).toHaveBeenCalledWith(404)
                expect(res.json).toHaveBeenCalledWith({ 'error': 'message' })
            });

            it('without message should return Not Found Error with custom message', () => {
                responseUtil.NotFoundData(res)

                expect(res.status).toHaveBeenCalledWith(404)
                expect(res.json).toHaveBeenCalledWith({ 'error': ResponseMessage.DataNotFound })
            });
        });

        describe('Fail Process Data', () => {
            it('with message should return Error 400', () => {
                responseUtil.FailProcess(res, 'message')

                expect(res.status).toHaveBeenCalledWith(400)
                expect(res.json).toHaveBeenCalledWith({ 'error': 'message' })
            });

            it('without message should return Error 400 with custom message', () => {
                responseUtil.FailProcess(res)

                expect(res.status).toHaveBeenCalledWith(400)
                expect(res.json).toHaveBeenCalledWith({ 'error': ResponseMessage.FailProcess })
            });
        });

        describe('Unauthorized Access', () => {
            it('with message should return Error 401', () => {
                responseUtil.UnauthorizedAccess(res, 'message')

                expect(res.status).toHaveBeenCalledWith(401)
                expect(res.json).toHaveBeenCalledWith({ 'error': 'message' })
            });

            it('without message should return Error 401 with custom message', () => {
                responseUtil.UnauthorizedAccess(res)

                expect(res.status).toHaveBeenCalledWith(401)
                expect(res.json).toHaveBeenCalledWith({ 'error': ResponseMessage.FailProcess })
            });
        });

        describe('Internal Server Error', () => {
            it('with message should return Error 500', () => {
                responseUtil.InternalServerError(res)

                expect(res.status).toHaveBeenCalledWith(500)
                expect(res.json).toHaveBeenCalledWith({ 'error': ResponseMessage.InternalServerError })
            });
        });

    });
});