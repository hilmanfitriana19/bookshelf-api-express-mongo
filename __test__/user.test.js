import request from 'supertest'
import app from '../src/app'
import { ResponseMessage } from '../src/utils/constant/response.message.constant';
import * as mongo from '../src/utils/test-utils'
import redisClient from '../src/cache/redis';

const offset = 0;
const limit = 2;

const userData1 = {
    "name": "user 1",
    "password": "user 1234",
    "email": "user1@info.com"
}

const userData2 = {
    "name": "user 2",
    "password": "user 1234",
    "email": "user2@info.com"
}
const userData3 = {
    "name": "user 3",
    "password": "user 1234",
    "email": "user3@info.com"
}

const userData1_ = {
    "name": "user 1",
    "email": "user1@info.com"
}

const userDataDetail1 = {
    "name": "user 1",
    "email": "user1@info.com",
    "books": []
}

let currentId = null

jest.mock('../src/middleware/authentication', () => ({
    JwtAuth: jest.fn((req, res, next) => next()),
}))

beforeAll(async () => {
    await mongo.connectDB()
    jest.spyOn(redisClient, 'get').mockImplementation((name) => { return null })

})

afterAll(async () => {
    await mongo.dropCollections()
    await mongo.disconnectDB()
})

describe('User API', () => {

    describe('Create User', () => {
        afterAll(async () => {
            await mongo.dropCollections();
        });

        describe('Using Correct Request Body', () => {
            it('must Return Created User', async () => {
                const result = await request(app).post('/user/')
                    .send(userData1)

                expect(result.status).toBe(201)
                expect(result.body.data).toMatchObject(userData1_)
                expect(result.body.message).toBe(ResponseMessage.SuccessCreated)
                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('name')
                expect(result.body.data).toHaveProperty('email')
            })
        })

        describe('Using Invalid Request Body Type', () => {
            it('Non Exisiting Name must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'password': 'test12345', 'email': 'email@mail.com' })

                expect(result.body.error).toBe('name is required')
                expect(result.status).toBe(400)
            })

            it('Non String Name must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'name': 12, 'password': 'test12345', 'email': 'email@mail.com' })

                expect(result.body.error).toBe('name must be string')
                expect(result.status).toBe(400)
            })

            it('Empty String must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'name': '', 'password': 'test12345', 'email': 'email@mail.com' })

                expect(result.body.error).toBe('name cannot be empty')
                expect(result.status).toBe(400)
            })

            it('Non Existing Password must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'name': 'test12345', 'email': 'email@mail.com' })

                expect(result.body.error).toBe('password is required')
                expect(result.status).toBe(400)
            })

            it('Password with least than 8 character must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'name': 'hello', 'password': 'test', 'email': 'email@mail.com' })

                expect(result.body.error).toBe('password must include minimum eight characters, at least one letter and one number')
                expect(result.status).toBe(400)
            })
            it('Password without number must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'name': 'hello', 'password': 'testemailbaru', 'email': 'email@mail.com' })

                expect(result.body.error).toBe('password must include minimum eight characters, at least one letter and one number')
                expect(result.status).toBe(400)
            })
            it('Password without character must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'name': 'hello', 'password': '12345678900', 'email': 'email@mail.com' })

                expect(result.body.error).toBe('password must include minimum eight characters, at least one letter and one number')
                expect(result.status).toBe(400)
            })
            it('Non Existing Email must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'password': 'test12345', 'name': 'nama' })

                expect(result.body.error).toBe('email is required')
                expect(result.status).toBe(400)
            })

            it('Non Email Structure must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'name': 'hello', 'password': 'hello 1234', 'email': 'emailmailcom' })

                expect(result.body.error).toBe('email is invalid')
                expect(result.status).toBe(400)
            })
            it('Empty String email must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ 'name': 'hello', 'password': 'hello 1234', 'email': '' })

                expect(result.body.error).toBe('email cannot be an empty')
                expect(result.status).toBe(400)
            })
        })

        describe('Using Exisiting Request Body', () => {
            it('Using Email already Taken must Return Error', async () => {
                const result = await request(app).post('/user/')
                    .send({ ...userData2, email: userData1.email })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe(ResponseMessage.EmailTaken)
            })
        })
    })

    describe('Get List User', () => {

        beforeAll(async () => {
            await mongo.db.collection('users').insertMany([userData1, userData2, userData3]);

        })

        afterAll(async () => {
            await mongo.dropCollections();
        });

        describe('Without Offset and Limit', () => {
            it('must Return List User', async () => {
                const result = await request(app).get('/user/')
                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Array)
                expect(result.body[0]).toHaveProperty('id')
                expect(result.body[0]).toHaveProperty('name')
                expect(result.body[0]).toHaveProperty('email')
                expect(result.body[0]).not.toHaveProperty('books')
                expect(result.body[0]).toMatchObject(userData1_)
                expect(result.body.length).toBe(3)
            })
        });

        describe('From Redis Cache', () => {

            afterAll(async () => {
                jest.spyOn(redisClient, 'get').mockImplementation((name) => { return null })
            })


            it('must Return List User', async () => {
                const mockRedis = jest.spyOn(redisClient, 'get').mockImplementation((name) => { return JSON.stringify([userData1, userData2]) })
                const result = await request(app).get('/user/')

                expect(mockRedis).toHaveBeenCalled()
                expect(mockRedis).toBeCalledWith('UserList_0_undefined')
                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Array)
                expect(result.body.length).toBe(2)
                expect(result.body[0]).toHaveProperty('_id')
                expect(result.body[0]).toHaveProperty('name')
                expect(result.body[0]).toHaveProperty('email')
                expect(result.body[0]).not.toHaveProperty('books')
                expect(result.body[0]).toMatchObject(userData1_)

            });
        });

        describe('Using Offset and Limit Data', () => {
            describe('Using Number in Offset and Limit', () => {
                it('must Return List User', async () => {
                    const result = await request(app).get('/user/')
                        .query({ offset, limit })

                    expect(result.status).toBe(200)
                    expect(result.body).toBeInstanceOf(Array)
                    expect(result.body[0]).toHaveProperty('id')
                    expect(result.body[0]).toHaveProperty('name')
                    expect(result.body[0]).toHaveProperty('email')
                    expect(result.body[0]).not.toHaveProperty('books')
                    expect(result.body.length).toBe(limit)
                })
            })

            describe('Using Non Number Offset', () => {
                it('must Return Error Message', async () => {
                    const result = await request(app).get('/user/')
                        .query({ offset: 'lima', limit })

                    expect(result.body.error).toEqual('offset must be number')
                    expect(result.status).toBe(400)
                })
            })
            describe('Using Non Number Limit', () => {
                it('must Return Error Message', async () => {
                    const result = await request(app).get('/user/')
                        .query({ offset, limit: 'lima' })

                    expect(result.body.error).toEqual('limit must be number')
                    expect(result.status).toBe(400)
                })
            })
        });

    })

    describe('Get List User Detail', () => {

        beforeAll(async () => {
            await mongo.db.collection('users').insertMany([userData1, userData2, userData3]);
        })

        afterAll(async () => {
            await mongo.dropCollections();
        });

        describe('Without Offset and Limit', () => {
            it('must Return List User', async () => {
                const result = await request(app).get('/user/detail')
                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Array)
                expect(result.body[0]).toHaveProperty('id')
                expect(result.body[0]).toHaveProperty('name')
                expect(result.body[0]).toHaveProperty('email')
                expect(result.body[0]).toHaveProperty('books')
                expect(result.body[0]).toMatchObject(userData1_)
                expect(result.body.length).toBe(3)
            })
        });

        describe('From Redis Cache', () => {

            afterAll(async () => {
                jest.spyOn(redisClient, 'get').mockImplementation((name) => { return null })
            })

            it('must Return List User', async () => {
                const mockRedis = jest.spyOn(redisClient, 'get').mockImplementation((name) => { return JSON.stringify([{ ...userDataDetail1 }]) })
                const result = await request(app).get('/user/detail')
                expect(mockRedis).toHaveBeenCalled()
                expect(mockRedis).toBeCalledWith('UserListDetail_0_undefined')
                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Array)
                expect(result.body.length).toBe(1)

                expect(result.body[0]).toHaveProperty('name')
                expect(result.body[0]).toHaveProperty('email')
                expect(result.body[0]).toHaveProperty('books')
                expect(result.body[0]).toMatchObject(userData1_)

            });
        });

        describe('Using Offset and Limit Data', () => {
            describe('Using Number in Offset and Limit', () => {
                it('must Return List User', async () => {
                    const result = await request(app).get('/user/detail')
                        .query({ offset, limit })

                    expect(result.status).toBe(200)
                    expect(result.body).toBeInstanceOf(Array)
                    expect(result.body[0]).toHaveProperty('id')
                    expect(result.body[0]).toHaveProperty('name')
                    expect(result.body[0]).toHaveProperty('email')
                    expect(result.body[0]).toHaveProperty('books')
                    expect(result.body.length).toBe(limit)
                })
            })

            describe('Using Non Number Offset', () => {
                it('must Return Error Message', async () => {
                    const result = await request(app).get('/user/detail')
                        .query({ offset: 'lima', limit })

                    expect(result.body.error).toEqual('offset must be number')
                    expect(result.status).toBe(400)
                })
            })
            describe('Using Non Number Limit', () => {
                it('must Return Error Message', async () => {
                    const result = await request(app).get('/user/detail')
                        .query({ offset, limit: 'lima' })

                    expect(result.body.error).toEqual('limit must be number')
                    expect(result.status).toBe(400)
                })
            })
        });

    })

    describe('Get One User By Id', () => {

        beforeAll(async () => {
            const user = await mongo.db.collection('users').insertOne({ ...userData1 });
            currentId = user.insertedId.toString()
        })

        afterAll(async () => {
            await mongo.dropCollections();
        })

        describe('Using Existing Id', () => {
            it('must Return User Data', async () => {
                const result = await request(app).get(`/user/${currentId}`)

                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Object)
                expect(result.body).toHaveProperty('id')
                expect(result.body).toHaveProperty('name')
                expect(result.body).toHaveProperty('email')
                expect(result.body).not.toHaveProperty('books')

            })
        });

        describe('Using Non Existing Id', () => {
            it('must Return Error 404', async () => {
                const result = await request(app).get(`/user/nonExistingId`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)

            })
        });
    })

    describe('Get One User Detail By Id', () => {

        beforeAll(async () => {
            const user = await mongo.db.collection('users').insertOne({ ...userData1 });
            currentId = user.insertedId.toString()
        })

        afterAll(async () => {
            await mongo.dropCollections();
        })

        describe('Using Existing Id', () => {
            it('must Return User Data', async () => {
                const result = await request(app).get(`/user/${currentId}/detail`)

                expect(result.status).toBe(200)
                expect(result.body).toBeInstanceOf(Object)
                expect(result.body).toHaveProperty('id')
                expect(result.body).toHaveProperty('name')
                expect(result.body).toHaveProperty('email')
                expect(result.body).toHaveProperty('books')

            })
        });

        describe('Using Non Existing Id', () => {
            it('must Return Error 404', async () => {
                const result = await request(app).get(`/user/nonExistingId/detail`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)

            })
        });
    })


    describe('Update User By Id', () => {

        beforeAll(async () => {
            const user = await mongo.db.collection('users').insertOne({ ...userData1 });
            await mongo.db.collection('users').insertOne({ ...userData2 });
            currentId = user.insertedId.toString()
        })

        afterAll(async () => {
            await mongo.dropCollections();
        })

        describe('Using Existing Id', () => {
            it('must Return Success Response and Updated Data', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'name': 'new Name', 'password': 'password123' })

                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)
                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('name')
                expect(result.body.data).toHaveProperty('email')
                expect(result.body.data).not.toHaveProperty('books')
                expect(result.body.data.name).toBe('new Name')
                expect(result.body.message).toBe(ResponseMessage.SuccessUpdated)
            })
        });

        describe('Using Non Invalid Request', () => {
            it('Non Exist Id must Return Error 404', async () => {
                const result = await request(app).patch(`/user/nonExistingId`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)
            })

            it('Update Email to Taken Email must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'email': userData2.email })

                expect(result.status).toBe(400)
                expect(result.body.error).toBe(ResponseMessage.EmailTaken)
            })

            it('Update Email to Non Valid Email must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'email': 'emailNotFalid' })

                expect(result.body.error).toBe('email is invalid')
                expect(result.status).toBe(400)
            })

            it('Update Email to Empty must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'email': '' })

                expect(result.body.error).toBe('email cannot be an empty')
                expect(result.status).toBe(400)
            })

            it('Update Password to Less than 8 character must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'password': 'aa1234' })

                expect(result.body.error).toBe('password must include minimum eight characters, at least one letter and one number')
                expect(result.status).toBe(400)
            })
            it('Update Password to With Digit Only must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'password': '1234567890' })

                expect(result.body.error).toBe('password must include minimum eight characters, at least one letter and one number')
                expect(result.status).toBe(400)
            })

            it('Update Password to With Letter Only must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'password': 'asdfghjkl' })

                expect(result.body.error).toBe('password must include minimum eight characters, at least one letter and one number')
                expect(result.status).toBe(400)
            })

            it('Update Password to Empty must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'password': '' })

                expect(result.body.error).toBe('password cannot be an empty')
                expect(result.status).toBe(400)
            })

            it('Update Name to Empty must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'name': '' })

                expect(result.body.error).toBe('name cannot be empty')
                expect(result.status).toBe(400)
            })

            it('Update Name to Non String must Return Error', async () => {
                const result = await request(app).patch(`/user/${currentId}`).send({ 'name': 123 })
                expect(result.body.error).toBe('name must be string')
                expect(result.status).toBe(400)
            })

        });
    })

    describe('Delete User By Id', () => {

        beforeAll(async () => {
            const user = await mongo.db.collection('users').insertOne({ ...userData1 });
            currentId = user.insertedId.toString()
        })

        afterAll(async () => {
            await mongo.dropCollections();
        })

        describe('Using Existing Id', () => {
            it('must Return Success Response and Deleted Data', async () => {
                const result = await request(app).delete(`/user/${currentId}`)

                expect(result.status).toBe(200)
                expect(result.body.data).toBeInstanceOf(Object)
                expect(result.body.data).toHaveProperty('id')
                expect(result.body.data).toHaveProperty('name')
                expect(result.body.data).toHaveProperty('email')
                expect(result.body.data).not.toHaveProperty('books')
                expect(result.body.data).toMatchObject(userData1_)
                expect(result.body.message).toBe(ResponseMessage.SuccessDeleted)
            })
        });
        describe('Using Non Exisiting Id', () => {
            it('Non Exist Id must Return Error 404', async () => {
                const result = await request(app).delete(`/user/nonExistingId`)

                expect(result.status).toBe(404)
                expect(result.body.error).toBe(ResponseMessage.DataNotFound)

            })
        })

    })
})