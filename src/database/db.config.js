import env from '../config/env'

export default {
    url: `${env.DATABASE_TYPE}://${env.DATABASE_HOST}:${env.DATABASE_PORT}/${env.DATABASE_NAME}?authSource=admin`
}