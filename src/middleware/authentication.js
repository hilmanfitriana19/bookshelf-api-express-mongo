import env from '../config/env';
import jwt, { JsonWebTokenError } from 'jsonwebtoken';
import { responseUtil } from '../utils/response/response.utils';
import { ResponseMessage } from '../utils/constant/response.message.constant';
import { User } from '../model/user.model';

export const JwtAuth = async (req, res, next) => {
    try {
        const accessToken = req.cookies.Authentication;
        const decoded = jwt.verify(accessToken, env.JWT_ACCESS_KEY);
        const user = await User.findById(decoded.user_id)
        if (!user) {
            return responseUtil.UnauthorizedAccess(res, ResponseMessage.InvalidToken)
        }
        req.user_id = user._id.toString()
        next()

    } catch (e) {
        if (e instanceof JsonWebTokenError && e?.message === `jwt must be provided`) {
            return responseUtil.UnauthorizedAccess(res, ResponseMessage.NoToken)
        } else if (e?.message === 'jwt expired') {
            return responseUtil.UnauthorizedAccess(res, ResponseMessage.TokenExpired)
        }
        return responseUtil.UnauthorizedAccess(res, ResponseMessage.InvalidToken)
    }
}